echo "Kubuntu installer"
##########################################################################################################################################################################################################
sudo apt-get install -y curl  build-essential git ffmpeg libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev libportmidi-dev libswscale-dev libavformat-dev libavcodec-dev zlib1g-dev mercurial libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsmpeg-dev libsdl1.2-dev libportmidi-dev libswscale-dev libavformat-dev libavcodec-dev zlib1g-dev python3-setuptools python3-wheel gcc playonlinux python3-setuptools python3-opengl python3-enchant python3-dev build-essential libgl1-mesa-dev libgles2-mesa-dev zlib1g-dev apt-transport-https cpu-checker qemu-kvm bridge-utils python-pip python-dev bochs nasm qemu virt-manager libxft-dev valgrind cmake boinc speech-dispatcher chipmunk-dev swig snap vlc blender wget openvpn ca-certificates unzip libavahi-compat-libdnssd-dev libpq-dev net-tools traceroute macchanger iproute2 certbot strongswan build-essential patch ruby-dev zlib1g-dev libsqlite libsqlite-dev libsqlite3 libsqlite3-dev liblzma-dev

##########################################################################################################################################################################################################
sudo snap install --classic heroku
##########################################################################################################################################################################################################
REQUIREMENTS_FILE="requirements.txt"
/bin/cat <<EOM >$REQUIREMENTS_FILE
Cython
pyttsx
PyAudio
PocketSphinx
SpeechRecognition
bleach
boto
boto3
botocore
bz2file
click
colorama
cryptography
docutils
fire
flashtext
Flask
future
gensim
html5lib
httplib2
idna
imbalanced-learn
jedi
Jinja2
jupyter-pip
jupyter
jupyter-console
jupyter-core
kappa
Keras
luminoth
lxml
nltk
notebook
numpy
pandas
pendulum
Pillow
pyasn1
pycrypto
pymongo
PyNaCl
PyOpenGL
PyQt5
PySDL2
pytz
PyYAML
requests
rsa
scikit-learn
scipy
selenium
setuptools
sip
six
SQLAlchemy
tensorflow
tensorflow-gpu
Theano
tornado
urllib3
wheel
xlwt
pyexcel
pyexcel-io
pyexcel-xls
pyexcel-xlsx
pyexcel-ods3
pyexcel-ods
pyexcel-xlsxw
pyexcel-xlsxr
pyexcel-odsr
pyexcel-htmlr
pyexcel-text
pyexcel-handsontable
pyexcel-pygal
pyexcel-sortable
pyexcel-gantt
nose
pyglet
chipmunk
EOM
python3 -m pip install -r $REQUIREMENTS_FILE
##########################################################################################################################################################################################################
sudo snap install rubymine --classic
sudo snap install pycharm-community --classic
sudo snap install intellij-idea-community --classic
##########################################################################################################################################################################################################
cd ~
mkdir `pwd`/Libraries
cd `pwd`/Libraries
mkdir Android_Sdk
wget http://www.portaudio.com/archives/pa_stable_v190600_20161030.tgz -q --show-progress
tar -xvzf pa_stable_v190600_20161030.tgz
cd portaudio
echo n | ./configure&& make
echo n | sudo make install
sudo apt-get update
##########################################################################################################################################################################################################
cd ~
cd `pwd`/Libraries
mkdir Downloads
cd Downloads
##########################################################################################################################################################################################################
wget https://dl.google.com/dl/android/studio/ide-zips/3.4.0.18/android-studio-ide-183.5452501-linux.tar.gz -q --show-progress
##########################################################################################################################################################################################################
wget https://www.qt.io/download-thank-you?hsLang=en -q --show-progress
##########################################################################################################################################################################################################
wget https://atom.io/download/deb -q --show-progress
##########################################################################################################################################################################################################
wget https://www.jetbrains.com/idea/download/download-thanks.html?platform=linux&code=IIC -q --show-progress
##########################################################################################################################################################################################################
wget https://www.jetbrains.com/pycharm/download/download-thanks.html?platform=linux&code=PCC -q --show-progress
##########################################################################################################################################################################################################
wget https://www.jetbrains.com/ruby/download/download-thanks.html?platform=linux -q --show-progress
##########################################################################################################################################################################################################
wget https://www.jetbrains.com/clion/download/download-thanks.html?platform=linux -q --show-progress
##########################################################################################################################################################################################################
wget https://www.gitkraken.com/download/linux-deb -q --show-progress
##########################################################################################################################################################################################################
cd /etc/openvpn
wget https://downloads.nordcdn.com/configs/archives/servers/ovpn.zip -q --show-progress
sudo unzip ovpn.zip
sudo rm ovpn.zip
cd ~
cd `pwd`/Libraries/Downloads/
wget https://nordvpn.com/api/files/zip -q --show-progress
##########################################################################################################################################################################################################
cd `pwd`
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn
##########################################################################################################################################################################################################
sudo gem install nokogiri rails pg devise rails_admin ahoy_matey materializecss bootstrap material_icons 
